#%%%%
# Graf
#%%%%

rm(list=ls())

require(foreach)
require(doSNOW)
require(caret)
require(pdp)
require(RANN)
require(abind)
require(RColorBrewer)
require(xtable)
require(PMCMR)
require(multcompView)
require(venn)


# DOWNLOAD DATA ####
  
dir_proj <- 'Projets/MiLan/analyse/stats/'
dir_out <- paste0(dir_proj, 'out/')

source(paste0(dir_proj, 'functions_MiLan.R'))


# Establish variables ----

redo <- T

impr2 <- expression('importance * ' * R^2)

# #---
# dn_data <- dimnames(arr_data)
# 
# nm_prot <- dn_data[[2]][grep('_PR', dn_data[[2]])]
# nm_envar <- dn_data[[2]][grep('_EV', dn_data[[2]])]
# 
# tot_grp_fct <- nm_prot[grep('^[a-z]', nm_prot)]
# 
# lscp <- nm_envar[grep('_PC', nm_envar, invert=T)]
# 
# # palette
# Set2 <- brewer.pal(9, 'Set2')
# 
# base_prot <- Set2[3:1]
# pal_prot <- c('grey', base_prot[c(1,2,3, 2,2,2,1,1,1,1,1,1,1,3,3,1,1,1,1,1,1,2,1)])
# names(pal_prot) <- nm_prot
# 
# base_envar <- grey(c(0.8,0))
# pal_envar <- base_envar[c(rep(1,11), rep(2,13))]
# names(pal_envar) <- nm_envar
# 
# pal_tot <- c(pal_prot, pal_envar)


##### LOOP THE SETSEED #####

for(i in 0:5){

  print(paste0('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'))
  print(paste0('%%%%%%%%%%%%  ',i,'  %%%%%%%%%%%%'))
  print(paste0('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'))
  
  # load data
  load(paste0(dir_out, '03_lst_mdl_', i, '.Rdata'))

  lscp <- dimnames(arr_data)[[2]]
  lscp <- lscp[!grepl('PC',lscp)][-1]
    
  # EXTRACT INFO FROM THE MODELS ####
  
  
  # Importance ----
  mat_imp <- as.data.frame(sapply(lst_mdl$tot, function(x) {
    
    # retreive the importance
    imp <- varImp(x)$importance
    
    # retreive the R2
    res <- x$results
    R2 <- res$Rsquared[res$mtry == unlist(x$bestTune)]
    
    impond <- imp * R2
    names(impond) <- NULL
    
    return(impond)
  }))
  
  row.names(mat_imp) <- dimnames(arr_data)[[2]][-1]
  
  # determination of the bufs_max_impond ---

  imp_lscp <- mat_imp[!grepl('PC', row.names(mat_imp)),]
  bst_buf <- rev(apply(imp_lscp[rank(rowMeans(imp_lscp)) %in% nrow(imp_lscp):(nrow(imp_lscp)-2),],
                       1, function(x) which(x == max(x))))
  
  
  # Pdp ----
  file <- paste0(dir_out, 'arr_pdp', i, '.Rdata')
  if(file.exists(file) & redo == F){
    load(file)
  } else {
  
    # prep cluster
    if(exists('cl')){
      stopCluster(cl)
    }
    
    cl <- makeSOCKcluster(5)
    
    clusterEvalQ(cl, library(foreach))
    clusterEvalQ(cl, library(doSNOW))
    
    registerDoSNOW(cl)
    
    lst_pdp <- foreach(j=as.character(buf), .verbose=T) %dopar% {
      
      print(paste('---', j, '---'))
  
      # prep cluster
      cl2 <- makeSOCKcluster(3)
      
      clusterEvalQ(cl2, library(pdp))
      clusterEvalQ(cl2, library(abind))
      
      registerDoSNOW(cl2)
      
      #---
      mod <- lst_mdl$tot[[j]]
      
      # pdp2 <- foreach(k = tot_grp_fct) %dopar% {
        
        pdp <- NULL
        for(l in lscp){
          partial <- partial(mod, l, grid.resolution=51)
          names(partial) <- c('X','Y')
          pdp <- abind(pdp, partial, along=3)
        }
        
        dimnames(pdp)[[3]] <- lscp
        
        return(pdp)
      }
      
      stopCluster(cl2)
      
      names(pdp2) <- tot_grp_fct
      
      return(pdp2)
    }
    
    names(lst_pdp) <- as.character(buf)
    
    # rearrange the list to an array
    arr_pdp <- lapply(lst_pdp, function(x) {
      abind(x, along=4)
    })
    
    arr_pdp <- abind(arr_pdp, along=5)
    
    #---
    save(arr_pdp, file=file)
  }
  
  dn_pdp <- dimnames(arr_pdp)
  
  
  # GRAF ####
  
  
  # # Impond per taxon (Figure S10) ----
  # 
  # X11(width=wdt2, heigh=wdt2*0.75)
  # par(mar=c(7,5,1,1))
  # 
  # tax <- t(na.omit(as.data.frame(mat_imp[,lscp,])))[,grep('^[A-Z]', dn_imp[[1]])]
  # tax <- tax[,order(colMeans(tax), decreasing=T)]
  # 
  # boxplot(tax, names=F, ylab=impr2)
  # mtext(sapply(colnames(tax), full_names), 1, 1, at=1:ncol(tax), col=pal_tot[colnames(tax)], las=2, cex=0.7)
  # 
  # lst_plot_graf[['Fig_S10']] <- recordPlot()
  # dev.off() ########### quelques différences, mais frosso merdo le même résultat
  # 
  # 
  # Impond detail ----
  # 
  # X11(width=wdt2*2.5, height=wdt2*1.5)
  # layout_Ys(4, 6, y=impr2, leg=list(leg_buf, leg_prot, leg_envar), mar=c(5,2,5,1))
  # 
  # ylim <- range(mat_imp)
  # 
  # # loop on prot
  # for(j in dn_imp[[2]]){
  #   plot_imp(mat_imp, ylim, j, line=4, bxp_buff=T)
  # }
  # 
  # lst_plot_graf[['impond_det']] <- recordPlot()
  # dev.off() #################### again, it moves a bit but nothing to worry too much (ok, 3rd bet lscp is now division rather then ed)
  
  
  # Pdp (Fig S6-9) ----
  
  for(j in dn_pdp[[4]][1]){
    
    X11(width=hei_pg, height=wdt2)
    # cairo_ps(paste0(dir_out, 'pdp_', j, '.eps'), width=hei_pg, height=wdt2)
    
    ind <- which(dn_pdp[[4]] == j)
    
    layout_Ys(3 ,5, y='Diversity', leg=list(leg_buf), mar=c(2,2,5,1))
    
    plot_pdp(lscp, j, arr_pdp, line=3, ylock=NULL, col.ttl=1, cex.main=0.9)
    
    lst_plot_graf[['Fig_pdp']][[j]] <- recordPlot()
    dev.off() ##################### slight difference but almost perfectly the same patterns
  } 
  
  
  # Fig diversity + lscp vs env + best div pdp (Fig 1) ----
  
  # cairo_ps(paste0(dir_out, 'Shannon.eps'), width=wdt2, height=wdt2*0.75)
  X11(width=wdt2, height=wdt2*0.75)
  
  # layout
  mat <- matrix(c(1,1,2,2, 0,5:11), ncol=4, byrow=T)
  mat <- rbind(mat[1,], cbind(mat, mat[,ncol(mat)]))
  mat[1,ncol(mat)] <- 3
  mat[1,ncol(mat)-1] <- 4
  
  layout(mat, width=c(0.2,1,1,0.5,0.5), height=c(1,1,0.2,1,1,1,1))
  par(oma=c(0,0,1,0))
  
  # lscp vs env (Fig 1A) ---
  
  par(mar=c(6,5,2,2))
  
  diff_Rsqrt <- apply(sapply(lst_mdl[-1], function(x) sapply(x, function(y) {
    rf <- y$tot_PR
    return(rf$results$Rsquared[rf$results$mtry == unlist(rf$bestTune)])
  })), 1, diff)
  
  barplot(diff_Rsqrt, col=pal_buf, las=2, axes=T, mgp=c(3.5,1,0),
          ylab=expression(Delta~R^2~'edaphic and topo-climatic vs landscape'),
          xlab='window size')
  
  letext(1, yshift=1.5)
  
  # impond (Fig 1B) ---
  
  par(mar=c(7,5,2,2))
  plot_imp(mat_imp, range(mat_imp['tot_PR',,]), 'tot_PR', ylab=impr2, cex.ylab=0.65, pch=19,
           cex.lab=0.75, col.ttl=F, title=F, bufs_max_imp=bufs_max_impond$tot_PR)
  
  letext(2, yshift=1.5)
  
  leg_buf(cex=0.75)
  leg_envar(cex=0.75)
  
  # pdp: find the best buffer for the thrre best variables in "fig diversity" (Fig 1C-F) ---
  
  for(j in names(bufs_max_impond$tot_PR)){
    par(mar=rep(0,4))
    plot.new()
    text(0.5,0.5, full_names(j), font=2, cex=1.5)
  }
  
  ind <- 3
  for(j in tot_grp_fct[1]){
    
    par(mar=rep(0,4))
    plot.new()
    text(0.5,0.5, full_names(j), srt=90)
    
    for(k in seq_along(bufs_max_impond$tot_PR)){
      par(mar=c(3,2,2,2))
      plot_pdp(names(bufs_max_impond$tot_PR)[k], j, arr_pdp, buf_pdp=bufs_max_impond$tot_PR[k], title=F)
      
      letext(ind, yshift=1.5, sub=k)
    }
    
    ind <- ind+1
  }
  
  lst_plot_graf[['Fig_1']] <- recordPlot()
  dev.off() ##################### ça a buger, il faut retrouver le bon set.seed
  
    
    # save the plots ----
  save(lst_plot_graf, wdt1, wdt2, hei_pg, file=paste0(dir_out, '04_lst_plot_gr', i, '.Rdata'))

}






























#%#%#
# models
#%#%#

rm(list=ls())

require(foreach)
require(doSNOW) # e.g. makeSOCKcluster()
require(caret)
require(abind)
require(xtable)
require(adespatial)


# DOWNLOAD DATA ####

dir_proj <- 'Projets/MiLan/analyse/stats/'
dir_out <- paste0(dir_proj, 'out/')

# load comm
source(paste0(dir_proj, 'functions_MiLan.R'))

load(paste0(dir_out, '00_lst_comm.Rdata'))
load(paste0(dir_out, '01_arr_met.Rdata'))
load(paste0(dir_out, '02_clim.Rdata'))
# #%%%%%%%%%
# load('/media/cseppey/HDD/home_bkup/bkup_220822_tromso_fin/cseppey/Projets/Potsdam/Landscape/stats/out/save/01_arr_met.Rdata')
# load('/media/cseppey/HDD/home_bkup/bkup_220822_tromso_fin/cseppey/Projets/Potsdam/Landscape/stats/out/save/02_clim.Rdata')
# dimnames(arr_met)[[1]][11] <- 'clumpi'
# #%%%%%%%%%


# infos ----

redo <- T

# lst_plot_03 <- NULL


# PROTIST PREPARATION ####

selec <- '04nzv'

mr <- lst_comm[[selec]]$mr
ass <- lst_comm[[selec]]$ass
taxo <- lst_comm[[selec]]$taxo


# Shannon calculation ----

div <- sapply(mr, diversity)


# PREPARE ENVAR (edaph + topo-clim + metlscp) ####

# edap topo
edap_topo <- lst_comm[[selec]]$env
edap_topo <- edap_topo[,-which(names(edap_topo) %in% c('X','Y','low_seq','flandcover'))]

# metric landscape
arr_met <- ifelse(is.na(arr_met), 0, arr_met)

# preselect the variables ----
lst_env <- list(env  = cbind.data.frame(edap_topo, clim[row.names(edap_topo),]),
                lscp = arr_met[,,row.names(edap_topo)])

names(lst_env$env) <- sub('^f', '', names(lst_env$env))

lst_env_unsc <- lst_env
save(lst_env_unsc, file=paste0(dir_out, '03_lst_env_unsc.Rdata'))

# fill empty values and scale ----
lst_env$env[which(is.infinite(as.matrix(lst_env$env)), arr.ind=T)] <- NA
lst_env$env <- predict(preProcess(lst_env$env, method='knnImpute'), newdata=lst_env$env)
lst_env$env <- predict(preProcess(lst_env$env, method=c('center', 'scale')), newdata = lst_env$env)

# # label the envar names
# names(lst_env$env) <- paste0(names(lst_env$env), '_EV')

# scal the lscp
lst_env$lscp <- apply(lst_env$lscp, c(1,2), scale)

# dimnames(lst_env$lscp)[[2]] <- paste0(dimnames(arr_met)[[1]], '_EV')
dimnames(lst_env$lscp)[[1]] <- row.names(edap_topo)

save(lst_env, file=paste0(dir_out, '03_lst_env.Rdata'))


# pca env ----
cairo_ps(paste0(dir_out, '03_PCA_env.eps'), width=wdt1, height=hei_pg)
# x11(wdt1, hei_pg)

par(mar=c(4,4,2,1), oma=c(0,0,1,0), mgp=c(2.5,1,0))
layout(matrix(1:3), height=c(0.75,1,1), respect=T)

ind_let <- 1
ysft <- 1.5

pca <- rda(lst_env$env)

# retreive enough axis to cover 80% of variation
cont <- summary(pca)$cont$importance
nb_axis <- which(cont[3,] > 0.8)[1]

axis <- pca$CA$u[,1:nb_axis]
var <- pca$CA$v[,1:nb_axis]
# colnames(axis) <- colnames(var) <- paste0('env_', colnames(axis))

eig <- cont[2,]

#---
plot(cont[3,], main='env', ylab='cumulative eigen values', xlab='')
abline(v=nb_axis, h=0.8)

ind_let <- letext(ind_let, yshift=ysft)

# plot
df <- rbind(axis,var)
for(j in list(c(1:2),c(3,4))){
  
  plot(NA, xlim=range(df[,j[1]])*1.2, ylim=range(df[,j[2]]), 
       xlab=paste0('PC', j[1], ' ', round(cont[2,j[1]], 2)),
       ylab=paste0('PC', j[2], ' ', round(cont[2,j[2]], 2)))
  
  # smp
  points(axis[,1], axis[,2], col='gold', pch=19, cex=0.5) 
  
  # var
  arrows(0,0,var[,j[1]],var[,j[2]], length=0, col='grey')
  text(var[,j], labels=row.names(var), cex=0.75)
  
  ind_let <- letext(ind_let, ysft)  
}

# loading
load <- matrix(c(loadings(princomp(lst_env$env))), nrow=ncol(lst_env$env))
row.names(load) <- names(lst_env$env)
  
# lst_plot_03[['PCA_env']][['eig_ordi']] <- recordPlot()

dev.off()

# scaling
axis <- predict(preProcess(axis, method=c('center', 'scale')), newdata = axis)

# colnames(axis) <- paste0(colnames(axis), '_EV')

save(axis, file=paste0(dir_out, '03_PCAs.Rdata'))

# loading
names(load) <- names(lst_env$env)

abs <- abs(load)

contrib <- apply(abs, 2, function(y) y/sum(y)*100)
contrib <- t(apply(contrib, 1, function(z) z*eig))[,1:nb_axis]
contrib <- rbind.data.frame(eigen=eig[1:nb_axis]*100, contrib)

save(contrib, file=paste0(dir_out, '03_contrib.Rdata'))
# lst_plot_03[['PCA_env']][['contrib']] <- contrib


# pca lscp ----

lscp_full_buf <- t(apply(lst_env$lscp, 1, rbind))

dn_lscp <- dimnames(lst_env$lscp)

colnames(lscp_full_buf) <- apply(expand.grid(dn_lscp[[2]], dn_lscp[[3]]), 1, function(x) paste(x, collapse='-'))

#---
pca_lscp <- rda(lscp_full_buf)
s <- summary(pca_lscp)

var <- s$species[,1:2]
site <- s$sites[,1:2]
cont <- s$cont$importance[2,1:2]

var_buf <- sapply(strsplit(row.names(var), '-'), '[[', 1)
centro <- sapply(as.data.frame(var), function(x) tapply(x, list(var_buf), mean))

#---

cairo_ps(paste0(dir_out, '03_PCA_lscp'), width=wdt2, height=wdt2)
# X11(width=wdt2, height=wdt2)

par(mar=c(3,3,1,1), mgp=c(2,0.75,0))

plot(NA, xlim=range(site[,1]), ylim=range(site[,2]), xlab=paste('PC1', round(cont[1], 2)), ylab=paste('PC2', round(cont[2], 2)))

points(site[,1:2], col='gold', pch=19, cex=0.5) 

for(i in 1:nrow(centro)){
  v <- row.names(centro)[i]
  
  for(j in as.character(buf)){
    coord <- var[row.names(var) == paste(v, j, sep='-'),]
    segments(centro[i,1], centro[i,2], coord[1], coord[2], col=pal_buf[j])
  }

  text(centro[i,1], centro[i,2], labels=sub('pland', '%_meadow', sub('pland_', '%_', v)), cex=0.75)
  
}

# lst_plot_03[['PCA_lscp']] <- recordPlot()

dev.off()


# RANDOM FORESTS ####

arr_data <- NULL
for(i in as.character(buf)){
  arr_data <- abind(arr_data, cbind(div, axis, lst_env$lscp[,,i]), along=3)
}

dimnames(arr_data)[[3]] <- buf

dn_data <- dimnames(arr_data)


# loop the models ----

file <- paste0(dir_out, '03_lst_mdl.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {
  
  lst_mdl <- NULL
  
  for(i in 0:5){
  
    set.seed(i)
   
    print(paste0('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'))
    print(paste0('%%%%%%%%%%%%  ',i,'  %%%%%%%%%%%%'))
    print(paste0('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'))
  
    # to make the varpart
    for(j in c('tot','PC','land')){
      
      print(paste('#####', j, '#####'))
      
      # prep cluster
      cl <- makeSOCKcluster(length(buf))
      
      clusterEvalQ(cl, library(caret))
      clusterEvalQ(cl, library(doSNOW))
      
      registerDoSNOW(cl)
      
      
      # loop on buffers
      lst_mdl[[as.character(i)]][[j]] <- foreach(k=as.character(buf), .verbose=T) %dopar% {
        
        print(paste('---', k, '---'))

        # prep_cluster 2
        cl2 <- makeSOCKcluster(floor(parallel::detectCores()/length(buf)))
        clusterEvalQ(cl2, library(caret))
        registerDoSNOW(cl2)

        #---
        Xs <- arr_data[,dn_data[[2]][-1],k]

        if(j == 'PC'){
          Xs <- Xs[,grep('PC', colnames(Xs))]
        } else if (j == 'land'){
          Xs <- Xs[,grep('PC', colnames(Xs), invert=T)]
        }

        Xs <- as.data.frame(Xs)

        # model ----
        data <- arr_data[,,k]

        f <- formula(paste('div~', paste(colnames(Xs), collapse='+')))

        # control of the bootstraps
        fold <- 10
        rep <- 3
        strata <- factor(floor((rank(div)-1)/(length(div))*fold+1))

        ctrl <- trainControl('repeatedcv', fold, rep)

        # run the models
        mod <- train(f, data=data, method='rf', tuneLength = 3, trControl=ctrl, strata=strata)


        #---

        stopCluster(cl2)

        return(mod)
      
      }
      
      print('check')
      names(lst_mdl[[as.character(i)]][[j]]) <- as.character(buf)
      
      stopCluster(cl)
    }
    
  }
  
  save(lst_mdl, arr_data, buf, pal_buf, file=file)
}

#















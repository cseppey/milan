#%%%%
# prep
#%%%%

rm(list=ls(all.names=T))

require(foreach)
require(doSNOW)
require(plotrix)
require(caret)
require(zCompositions)

source('bin/src/my_prog/R/pie_taxo.r')

# prep cluster

nb_cpu <- parallel::detectCores()

cl <- makeSOCKcluster(nb_cpu)

clusterEvalQ(cl, library(raster))
clusterEvalQ(cl, library(rgeos))
clusterEvalQ(cl, library(landscapemetrics))

registerDoSNOW(cl)

#--- 
redo <- T


# DOWNLOAD DATA ####

dir_proj <- 'Projets/MiLan/analyse/stats/'
dir_in <- paste0(dir_proj, 'data/')
dir_out <- paste0(dir_proj, 'out/')
dir.create(dir_out, showWarnings=F)

source(paste0(dir_proj, 'functions_MiLan.R'))


# comm ----

file <- paste0(dir_out, '00_mr_ass.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {
  # to speedup the process, .mr is chuncked in 4
  
  # nb_otu=`head -n1 preAlps_11.mr | wc -w`
  # chk_sz=$(((nb_otu)/4))
  # echo $nb_otu $chk_sz
  # for i in {1..4}
  # do
  #   fst=$((chk_sz*(i-1)+2))
  #   snd=$((fst+chk_sz-1))
  #   if ((snd > nb_otu))
  #   then
  #     snd=$nb_otu
  #   fi
  #   cut -f1,${fst}-${snd} preAlps_11.mr > preAlps_11_$i.mr
  # done
  
  system.time(lst_mr <- foreach(i=1:4, .verbose=T) %dopar% {
    read.table(paste0(dir_in, 'MTB/preAlps_11_', i, '.mr'), h=T)
  }) # 14 sec
  mr_00tot <- cbind.data.frame(lst_mr)
  
  system.time(ass_00tot <- read.table(paste0(dir_in, 'MTB/preAlps_12.ass'), sep='\t', stringsAsFactors = T)) # 2 sec
  
  save(mr_00tot, ass_00tot, file=file)
}

corresp_00tot <- read.table(paste0(dir_in, 'Antoine_Fill.csv'), h=T, sep='\t')


# env ----

topo <- read.table(paste0(dir_in, 'Env_Bioassemble.csv'), h=T, row.names=1, sep='\t')
topo2 <- read.table(paste0(dir_in, 'sites_env.txt'), h=T, row.names=1, sep=',')
topo[as.character(topo2$sites_txt_Events),c(3:10,12:16)] <- topo2[,4:16]

edap <- read.table(paste0(dir_in, 'Alps2013-Soil-chemical_Global_table_2013.csv'), h=T, row.names=2, sep='\t')


# ORGANIZE DATA ####

# suppress null samples and aggregate samples according to plot: 00tot -> 01agg
# remove rest of macro-organisms: 01agg -> 02nmc

# mr ----
row.names(mr_00tot) <- corresp_00tot$UNIL
ind_203 <- which(row.names(mr_00tot) == '---')
ind <- c(1:(ind_203-1), nrow(mr_00tot), (ind_203+1):(nrow(mr_00tot)-1))

mr_00tot <- mr_00tot[ind,]
corresp_00tot <- corresp_00tot[ind,]
  
# corresp
spl_corr <- strsplit(as.character(corresp_00tot[,2]), '.', fixed=T)
corresp_00tot <- cbind.data.frame(UNINE=corresp_00tot[,1], plot_nb=as.numeric(sapply(spl_corr, '[[', 1)),
                                  info_plot=sapply(spl_corr, function(x) ifelse(length(x) == 1, '', x[2])),
                                  corresp_00tot[,3:ncol(corresp_00tot)])

ech_nnull <- ifelse(corresp_00tot$X1 == 'N' & corresp_00tot$X2 == 'N' & corresp_00tot$X3 == 'N', F, T)

corresp_00tot <- cbind.data.frame(corresp_00tot, ech_nnull)
corresp_01agg <- corresp_00tot[corresp_00tot$ech_nnull,]

# aggregate the multiple samplings

file <- paste0(dir_out, '00_mr_01agg.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {
  mr_01agg <- mr_00tot[corresp_00tot$ech_nnull == T,]
  mr_01agg <- mr_01agg[,colSums(mr_01agg) != 0]
  
  tb_plot <- table(corresp_01agg$plot_nb)
  
  nm_mltp_smp <- names(tb_plot)[tb_plot > 1]
  lst_01agg <- foreach(i=nm_mltp_smp, .verbose=T) %dopar% {
    print(i)
    id_smp <- which(corresp_01agg$plot_nb == i)
    
    if(tb_plot[i] == 2){
      agg <- floor(colMeans(mr_01agg[id_smp,]))
    } else {
      agg <- sapply(mr_01agg[id_smp,], median)
    }
    
    return(list(agg=agg,id_smp=id_smp))
    
    rm(agg, id_smp)
    gc()
  }
  
  names(lst_01agg) <- nm_mltp_smp
  
  mr_01agg <- rbind.data.frame(mr_01agg[-unlist(sapply(lst_01agg, '[[', 'id_smp')),], as.data.frame(t(sapply(lst_01agg, '[[', 'agg'))))
  row.names(mr_01agg) <- sapply(strsplit(row.names(mr_01agg), '.', fixed=T), '[[', 1)
  mr_01agg <- mr_01agg[order(as.numeric(row.names(mr_01agg))),colSums(mr_01agg) != 0]
  
  save(mr_01agg, file=file)  
  
}


# ass ----

names(ass_00tot) <- c('OTU_id','pid','e_value','taxo','GbId','seq')
row.names(ass_00tot) <- paste0('X', sapply(strsplit(as.character(ass_00tot$OTU_id), '_'), '[[', 1))

ass_00tot$taxo <- sub('Haptoria_1','Haptoria',ass_00tot$taxo)

ass_01agg <- ass_00tot[names(mr_01agg),]

#---
cs <- log(colSums(mr_01agg))
cs <- cs[cs != 0]
cs <- (cs-min(cs)) / diff(range(cs))

file <- paste0(dir_out, '00_OTU_assignation.eps')
if(!file.exists(file) | redo == T){
  cairo_ps(file)
  
  plot(ass_01agg$pid~log(ass_01agg$e_value), pch=19, cex=0.1, col=grey(cs), xlab='log e-value', ylab='perc id')
  abline(h=60, v=0)
  
  dev.off()
}

# taxo
taxo_00tot <- as.data.frame(matrix(unlist(strsplit(as.character(ass_00tot$taxo), ';')), ncol=8, byrow=T))
taxo_00tot <- as.data.frame(lapply(taxo_00tot, as.factor))
dimnames(taxo_00tot) <- list(row.names(ass_00tot), c('reign','phylum','division','order','class','family','genus','species'))

#---
taxo_01agg <- taxo_00tot[row.names(ass_01agg),]

# remove rest of macroorg ---
macro <- grepl('Metazoa|Embryophyceae|Fungi', ass_01agg$taxo)

mr_02nmc <- mr_01agg[,!macro]
ass_02nmc <- ass_01agg[!macro,]
taxo_02nmc <- droplevels(taxo_01agg[!macro,])


# env ----

# aggregate the edaphic
edap$plot_nb <- factor(sapply(strsplit(row.names(edap), '.', fixed=T), '[[', 1))

edap$rh <- apply(edap[,c("Dry_soil_weight","Wet_soil_weight")], 1, function(x) diff(x)/x[2])
edap$C_N <- apply(edap[,c('C','N')], 1, function(x) x[1]/x[2])

#---
vars <- c(9,13:14,17:19,26:48,53:55,57)
edap_01agg <- NULL

for(i in levels(edap$plot_nb)){
  ind_smp <- which(edap$plot_nb == i)
  if(length(ind_smp) == 3){
    edap_01agg <- rbind.data.frame(edap_01agg, apply(edap[ind_smp,vars], 2, median))
  } else if(length(ind_smp) == 2){
    edap_01agg <- rbind.data.frame(edap_01agg, apply(edap[ind_smp,vars], 2, mean))
  } else {
    edap_01agg <- rbind.data.frame(edap_01agg, edap[ind_smp,vars])
  }
}
dimnames(edap_01agg) <- list(levels(edap$plot_nb), names(edap)[vars])

edap_01agg <- edap_01agg[row.names(mr_01agg),]

# select the topo
env_01agg <- env_02nmc <- cbind.data.frame(edap_01agg, topo[row.names(mr_01agg),])


# cleanup ----

# small samples:      02nmc -> 03nls
# nearZeroVariance:   03nls -> 04nzv
# centered log ratio: 04nzv -> 05clr

# remove small samples (piecewise linear)
lrs <- log(sort(rowSums(mr_02nmc)))

x <- brks <- seq_along(lrs)

mse <- lh_steep <- NULL
for(j in brks){
  mod <- lm(lrs~x*(x <= brks[j]) + x*(x < brks[j]))
  mse <- c(mse, summary(mod)$sigma)
  
  co <- coef(mod)
  lh_steep <- c(lh_steep, co[2]+co[5] > co[2])
}

min_mse <- which(mse == min(mse[lh_steep], na.rm=T))

mod <- lm(lrs ~ x*(x < min_mse) + x*(x > min_mse))
co <- coef(mod)

low_seq <- names(lrs)[1:(min_mse-1)]

env_01agg$low_seq <- env_02nmc$low_seq <- row.names(env_01agg) %in% low_seq

# plot
cairo_ps(paste0(dir_out, '00_piecewise.eps'), wdt2, wdt2)

par(mar=c(5,4,2,4))

plot(lrs, ylab='log number of reads')
abline(co[1]+co[3], co[2]+co[5])
abline(co[1]+co[4], co[2])
abline(v=min_mse, lty=2, col=2)

mse2 <- (mse-min(mse))/diff(range(mse)) * diff(range(lrs)) + min(lrs)
lines(1:length(mse2), mse2, col=3)

lgt <- 6
lab <- seq(min(mse), max(mse), length=lgt)
at <-(lab-min(mse))/diff(range(mse)) * diff(range(lrs)) + min(lrs) 

axis(4, at, round(lab, 2))
mtext('mean square error\npiecewise linear model', 4, 3)

dev.off()

#---
env_03nls <- env_02nmc[!env_02nmc$low_seq,]
mr_03nls <- mr_02nmc[!env_02nmc$low_seq,]
mr_03nls <- mr_03nls[,colSums(mr_03nls) != 0]
ass_03nls <- ass_02nmc[names(mr_03nls),]
taxo_03nls <- droplevels(taxo_02nmc[names(mr_03nls),])

# remove nearZeroVar OTUs
file <- paste0(dir_out, '00_nzv.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {
  system.time(nzv <- nearZeroVar(mr_03nls, foreach=T, allowParallel = T)) # nzv from mr_03clr gives nothing
  save(nzv, file=file)
}

#---
mr_04nzv <- mr_03nls[,-nzv]
ass_04nzv <- ass_03nls[names(mr_04nzv),]
taxo_04nzv <- droplevels(taxo_03nls[names(mr_04nzv),])
env_04nzv <- env_03nls

#---
lst_comm <- list('00tot'=list(               mr=mr_00tot, ass=ass_00tot, taxo=taxo_00tot),
                 '01agg'=list(env=env_01agg, mr=mr_01agg, ass=ass_01agg, taxo=taxo_01agg),
                 '02nmc'=list(env=env_02nmc, mr=mr_02nmc, ass=ass_02nmc, taxo=taxo_02nmc),
                 '03nls'=list(env=env_03nls, mr=mr_03nls, ass=ass_03nls, taxo=taxo_03nls),
                 '04nzv'=list(env=env_04nzv, mr=mr_04nzv, ass=ass_04nzv, taxo=taxo_04nzv))


# pie chart ----

lst_pie <- NULL
for(i in seq_along(lst_comm)){
  
  mr <- decostand(lst_comm[[i]]$mr, 'total')
  taxo <- lst_comm[[i]]$taxo
  
  nm <- names(lst_comm)[i]
  
  #---
  cairo_ps(paste0(dir_out, 'pie_', names(lst_comm)[i], '.eps'), width=wdt2, height=wdt2)
  
  par(xpd=NA)
  
  pie <- pie_taxo(mr, taxo, 1:5, last_tax_text=F, cex=1, mat_lay=matrix(c(0,1,0, 0,0,0, 2,2,2), 3),
                  wdt_lay=c(1,0.3,3), hei_lay=c(1,1.5,1), rshift=0.2, oma=c(2,2,2,2))

  dev.off()
  
  lst_pie[[i]] <- pie
  
}


#---
save(lst_comm, lst_pie, wdt1, wdt2, hei_pg, buf, pal_buf, file=paste0(dir_out, '00_lst_comm.Rdata'))

#####















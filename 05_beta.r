#%#%#
# beta
#%#%#

rm(list=ls())

require(doSNOW)
require(abind)
require(xtable)
require(venn)

dir_proj <- 'Projets/MiLan/analyse/stats/'
dir_out <- paste0(dir_proj, 'out/')

source(paste0(dir_proj, 'functions_MiLan.R'))

# load data ####
load(paste0(dir_out, '00_lst_comm.Rdata'))
load(paste0(dir_out, '01_arr_met.Rdata'))
load(paste0(dir_out, '03_lst_env.Rdata'))
load(paste0(dir_out, '03_PCAs.Rdata'))
load(paste0(dir_out, '03_lst_mdl_2.Rdata'))

# plot saves
lst_plot_04 <- NULL

redo <- F

# get the dataset
selec <- '04nzv'

mr <- lst_comm[[selec]]$mr
env <- lst_env$env
lscp <- lst_env$lscp
coord <- data.frame(x=lst_comm[[selec]]$env$X, y=lst_comm[[selec]]$env$Y)

elev <- lst_comm[[selec]]$env$fmnt25
elev_rnd <- round((elev-min(elev))/diff(range(elev))*999+1)

pal_elev <- colorRampPalette(c('pink','skyblue'))(1000)
pal_elev <- pal_elev[elev_rnd]

dis_bray <- vegdist(mr)
dis_env <- dist(env)
dis_lscp <- lapply(as.character(buf), function(x) dist(lscp[,,x]))
names(dis_lscp) <- as.character(buf)
dis_spat <- dist(coord)

lst_dis <- list(dis_bray=dis_bray, dis_env=dis_env, dis_spat=dis_spat)
lst_dis <- append(lst_dis, dis_lscp)

# Varpart -----------------------------------------------------------------

env_df <- as.data.frame(axis)
names(env_df) <- sapply(names(env_df), trim_tag)

spat_df <- as.data.frame(pcnm(dis_spat)$vectors)

var_typ <- c('lscp','env','spat')

# selection of the PCNM variables
rda0 <- capscale(dis_bray~1, data=spat_df)
rda1 <- capscale(dis_bray~., data=spat_df)

file <- paste0(dir_out, '04_ordi.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {
  set.seed(0)
  ordi <- ordistep(rda0, formula(rda1))
  save(ordi, file=file)
}

pcnm_sel <- row.names(ordi$CCA$biplot)
spat_sel_df <- spat_df[,pcnm_sel]

var_sel <- c(var_typ[1:2], 'spat_sel')


# cluster
if(exists('cl')){
  stopCluster(cl)
}
cl <- makeSOCKcluster(5)
registerDoSNOW(cl)

file <- paste0(dir_out, '04_lst_RDA.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {
  
  n_rda <- c(as.character(buf),'null')
  
  lst_RDA <- foreach(i=n_rda) %dopar% {
    
    if(i %in% as.character(buf)){
      
      lscp_df <- as.data.frame(lscp[row.names(mr),,i])
      names(lscp_df) <- sapply(names(lscp_df), trim_tag)
      
      data <- data.frame(lscp_df, env_df, spat_sel_df)
      
      varpart.obj <- varpart(dis_bray, lscp_df, env_df, spat_sel_df)
    
      res.vec <- varpart.obj$part$fract$Adj.R.square
      names(res.vec) <- c(var_typ, apply(combn(var_typ, 2), 2, function(x) paste(x, collapse='_')), "all")
      
    } else {
      
      data <- data.frame(env_df, spat_sel_df)
      
      varpart.obj <- varpart(dis_bray, env_df, spat_sel_df)
      
      res.vec <- varpart.obj$part$fract$Adj.R.square
      names(res.vec) <- c(var_typ[-1], 'all')
      
    }    
    
    
    # RDA
    name_vars <- eval(parse(text=
                              paste('c(', 
                                    paste0(var_sel, '_var = paste(names(', var_sel, '_df), collapse=\" + \")',
                                           collapse=', ')
                                    , ')')
    ))
    
    if(i == 'null'){
      name_vars <- name_vars[-1]
    }
    
    RDA <- capscale(formula(paste('dis_bray ~', paste(name_vars, collapse=' + '))), data=data)
    
    # find significant variables
    set.seed(0)
    a <- anova.cca(RDA, by='margin', parallel=16, permutations = 999)
    
    
    # RDA partial out
    lst_frac <- NULL
    for(j in seq_along(name_vars)){
      
      l <- NULL
      for(k in c('margin','term')){
        
        f <- formula(paste('dis_bray ~', name_vars[[j]], '+ Condition(', paste(name_vars[-j], collapse=' + '), ')'))
        if(k == 'term'){
          f <- formula(paste('dis_bray ~', name_vars[[j]]))
        }
        
        aFrac <- capscale(f, data=data)
        
        aFrac.anova <- anova(aFrac)
        
        l[[k]] <- list(rda=aFrac, anova=aFrac.anova)
      }
      
      lst_frac[[j]] <- l
    }
    

    if(i %in% as.character(buf)){
      names(lst_frac) <- var_typ
    } else {
      names(lst_frac) <- var_typ[-1]
    }
    
    # saves
    l = list(varpart.obj, res.vec, lst_frac, RDA, a)
    names(l) = c("varpart", "R2adj", "lst_frac", 'rda', 'anova')
    
    return(l)
    
  }

  names(lst_RDA) <- n_rda
  
  save(lst_RDA, file=file)
}

# explicative power
sapply(lst_RDA, function(x) x$R2adj)


# table anova RDA

tb_var <- sapply(lst_RDA, function(x) as.matrix(x$anova)[,'Pr(>F)'])
tb_var <- as.data.frame(t(sapply(names(tb_var[[1]]), function(x) sapply(tb_var, function(y) {
  pv <- y[names(y) == x]
  ifelse(length(pv), pv, NA)
}))))
tb_var <- tb_var[-nrow(tb_var),]

# signif of the fractions
tb_frac <- sapply(lst_RDA, function(x) sapply(x$lst_frac, function(y) sapply(y, function(z) z$anova$`Pr(>F)`[1])))
tb_frac$null <- cbind(lscp=c(NA,NA), tb_frac$null)

#---
lst_plot_04[['RDA']][['tb_var']] <- tb_var
lst_plot_04[['RDA']][['tb_frac']] <- tb_frac

# plot RDA ----

venn(3)
usr_venn <- par('usr')

v <- apply(expand.grid(0:1,0:1,0:1), 1, function(x) {
  gz <- getZones(paste(x, collapse=''), 'A,B,C')
  gc <- getCentroid(gz)[[1]]
  return(list(gz[[1]],gc))
})

rng <- apply(abind(lapply(lapply(v, '[[', 1), function(x) as.data.frame(sapply(x, function(y) range(y, na.rm = T)))), along=3), 2, range)
rng <- apply(rng, 2, function(x) x+diff(x)*0.1*c(-1,1))

cntr <- as.data.frame(t(sapply(v, '[[', 2)))
cntr <- cntr[-nrow(cntr),]

dev.off()



#---
x11(width=wdt2, height=hei_pg)

layout(cbind(rep(1,5), matrix(2:11, ncol=2, byrow=T)), width=c(0.75,1,1), respect=T)

# leg
par(mar=rep(0,4), oma=c(4,0,1,0))

plot.new()
usr=par('usr')
# box('figure')

x <- 0.25
ys <- usr[3:4]+diff(usr[3:4])*0.4*c(1,-1) 

seq_y <- seq(ys[1], ys[2], length.out=1000)
seq_lab <- signif(seq(min(elev), max(elev), length.out=5),2)

points(rep(x+0.15, 1000), seq_y, col=colorRampPalette(c('pink','skyblue'))(1000))
text(x, seq(ys[1], ys[2], length.out=5), seq_lab, pos=2, offset=0)

text(x, ys[2]+diff(usr[3:4])*0.05, 'elevation\n(m asl)')

#---
rda_lim <- sapply(1:2, function(x) range(unlist(lapply(lapply(lst_RDA, function(y) 
  as.data.frame(apply(summary(y$rda)$sites[,1:2], 2, range))), '[[', x))))

par(mgp=c(2,0.75,0))
ind <- 1
for(i in seq_along(buf)){

  par(mar=c(0,0,0,0))
  
  # rda ---  
  RDA <- lst_RDA[[i]]$rda
  s <- summary(RDA)
  
  a <- lst_RDA[[i]]$anova
  
  selec_var <- s$biplot[row.names(s$biplot) %in% na.omit(row.names(a)[a$`Pr(>F)` <= 0.05]),]
  col_var <- rep('grey30', nrow(selec_var))
  col_var[row.names(selec_var) %in% trim_tag(dimnames(lscp)[[2]])] <- 'black'
  
  # plot
  rda_coord <- lapply(1:2, function(x) {
    if(s$sites[row.names(s$sites) == '491',x] < 0){
      ind <- 1
    } else {
      ind <- -1
    }
    return(list(s$sites[,x]*ind, selec_var[,x]*ind))
  })
  
  var <- round(s$cont$importance[2,1:2], 2)
  
  #---
  plot(NA, xlim=rda_lim[,1], ylim=rda_lim[,2], xlab='', xaxt='n', ylab='', xpd=NA)
  
  if(i == 5){
    axis(1)
    mtext(paste('RDA1',var[1]), 1, 2, cex=0.75)
  }
  
  if(i == 3){
    mtext(paste('RDA2',var[2]), 2, 2, cex=0.75)
  }
  
  box('figure')  
  
  sites <- sapply(rda_coord, '[[', 1)
  sel_var <- sapply(rda_coord, '[[', 2)*2
  
  points(sites, pch=19, col=pal_elev)
  
  arrows(0,0, sel_var[,1], sel_var[,2], length=0, col='grey50')
  text(sel_var, labels=row.names(sel_var), col=col_var, cex=0.75)

  ind <- letext(ind, xshift=-1.5, yshift=-1)
    
  # varpart ---
  adj_R2 <- round(lst_RDA[[i]]$varpart$part$indfract$Adj.R.square, 2)[-8]

  #---
  # par(mar=c(4,1,1,4))
  
  plot.new()
  plot.window(rng[,1], rng[,2])
  box()
  box('figure')
  
  usr <- par('usr')
  for(j in c(2,3,5,8)){
    polygon(v[[j]][[1]][[1]],v[[j]][[1]][[2]])
  }
  
  mid_x <- diff(usr[1:2])*0.5
  mid_y <- diff(usr[3:4])*0.5
  text(c(mid_x+diff(usr[1:2])*0.45*c(-1,1), mid_x), 
       c(rep(mid_y+diff(usr[3:4])*0.2,2), usr[4]-diff(usr[3:4])*0.1),
       c('lscp','spat','env'), pos = c(4,2,3), cex=0.75)

  text(cntr, labels = adj_R2[c(7,1,2,4,3,6,5)], cex=0.75)
  
}
  
lst_plot_04[['RDA']][['plot']] <- recordPlot()

dev.off()


# DDR ---------------------------------------------------------------------


# loop the y dis mat
combi <- NULL
for(i in names(lst_dis)[-3]){
  
  # loop the x dis mat
  for(j in names(lst_dis)[-1]){
    
    if((i == 'dis_env' | grepl('0', i)) & j != 'dis_spat' ) next
    
    combi <- rbind(combi, c(i,j))
  }
}


#---
nb_perm <- 1000

# change 0 to very small number

file <- paste0(dir_out, '04_lst_DDR.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {
  
  # cluster
  if(exists('cl')){
    stopCluster(cl)
  }
  cl <- makeSOCKcluster(3)
  clusterEvalQ(cl, library(doSNOW))
  registerDoSNOW(cl)
  
  lst_DDR <- foreach(h=1:nrow(combi), .verbose=T) %dopar% {
    
    # get the matrix
    y <- combi[h,1]
    x <- combi[h,2]
    dis_y <- lst_dis[[y]]
    dis_x <- lst_dis[[x]]
    
    dis_y <- simili0(dis_y)
    dis_x <- simili0(dis_x)
    
    
    # 0 to buffer or buffer windows
    lst_ddr <- NULL
    for(i in c('zero', 'window')){
      
      if(i == 'zero'){
        fnc_ind <- function(x) which(dis_spat < buf[x])
      } else {
        fnc_ind <- function(x) which(dis_spat > ifelse(x == 1, 0, buf[x-1]) & dis_spat < buf[x])
      }
      
      # cluster
      cl2 <- makeSOCKcluster(5)
      clusterEvalQ(cl2, library(abind))
      registerDoSNOW(cl2)
      
      # select samples pairs according to the buffer
      lst_buf <- foreach(j = c(0,seq_along(buf))+1) %dopar% {
        
        if(j <= length(buf)){
          index <- fnc_ind(j)
        } else {
          index <- seq_along(dis_spat)
        }
        
        # random model or bootstrap model
        mod_type <- c('rnd','boot')
        arr_perm <- NULL
        for(k in mod_type){
          
          # permut
          res_perm <- NULL
          for(l in 1:nb_perm){
            
            set.seed(l)
            
            ind_y <- index
            ind_x <- index
            
            # random
            if(k == 'rnd'){
              ind_y <- sample(length(dis_y))[index]
            }
            
            # boot 80%
            if(k == 'boot'){
              ind_y <- ind_x <- sample(index, size = length(index)*0.8)
            }
            
            lm <- lm(log(dis_y[ind_y])~log(dis_x[ind_x]))
            
            res_perm <- rbind(res_perm, c(coef(lm)[1], coef(lm)[2], 
                                          summary(lm)$adj.r.squared, anova(lm)$'Pr(>F)'[1]))
            
          }
          
          colnames(res_perm) <- c('intercept','slp','r2adj','pv')
          
          arr_perm <- abind(arr_perm, res_perm, along=3)
        }
        
        dimnames(arr_perm)[[3]] <- mod_type
        
        # lm within buf
        lst_out <- list(arr_perm=arr_perm, fit=lm(log(dis_y[index])~log(dis_x[index])))
        
        return(lst_out)
      }
      
      names(lst_buf) <- c(as.character(buf), 'tot')
      
      lst_ddr[[i]] <- lst_buf 
      
      stopCluster(cl2)
      
    }  
    
    return(lst_ddr)
  }
  
  nm <- apply(combi, 1, function(x) paste(x, collapse='_'))
  names(lst_DDR) <- gsub('dis_', '', nm)
  
  save(lst_DDR, file=file)
  
}

lst_plot_04[['tab_window']] <- sapply(lst_DDR[8:13], function(x) c(coef(x$window$tot$fit), pv=anova(x$window$tot$fit)$`Pr(>F)`[1]))


# # graf ----
# pal_buf_smp <- rep('grey80', length(dis_spat))
# # for(i in seq_along(buf)){
# #   pal_buf_smp[dis_spat > ifelse(i == 1, 0, buf[i-1]) & dis_spat <= buf[i]] <- pal_buf[i]
# # }
# # pair_wn_buf <- dis_spat <= 2000
# pair_wn_buf <- dis_spat <= 0
# 
# par(mfrow=c(5,3), mar=c(4,4,1,1), mgp=c(2,0.7,0))
# for(i in names(lst_DDR)){
#   
#   yx <- strsplit(i, '_')[[1]]
#   
#   dis_y <- lst_dis[[grep(yx[1], names(lst_dis))[1]]]
#   dis_x <- lst_dis[[grep(yx[2], names(lst_dis))[1]]]
# 
#   plot(log(dis_y)~log(dis_x), xlab=yx[2], ylab=yx[1], pch=19, cex=ifelse(pair_wn_buf, 1, 0.2), col=pal_buf_smp)
# 
#   # for(j in as.character(buf)){
#   #   
#   #   apply(lst_DDR[[i]]$window[[j]]$arr_perm[,c('intercept','slp'),'boot'], 1, function(x) {
#   #     abline(x[1], x[2], col=pal_buf[j], lwd=0.5, lty=3)
#   #   })
#   #   
#   #   abline(coef(lst_DDR[[i]]$window[[j]]$fit))
#   # }
# }
# 
# dev.off()


# Fig DDR ####

lm <- lst_DDR$bray_spat$window$tot$fit

coef <- coef(lst_DDR$bray_spat$window$tot$fit)
r2 <- summary(lst_DDR$bray_spat$window$tot$fit)$adj.r.squared

#---
x11(width=wdt1, height=wdt2*0.75)
layout(matrix(c(1,1, 2,2, 2,3), byrow=T, ncol=2), width=c(1,0.35), height=c(1,0.4,0.6))

ind_let <- 1

xshift <- 2.5
yshift <- -0.5

#---
par(mar=c(0,4,0,0), mgp=c(2,0.75,0), oma=c(4,0,1,1))

plot(log(dis_bray)~log(dis_spat), pch=19, cex=0.2, xaxt='n',
     col='gray', xlab='', ylab='log of Bray Curtis distance')
usr <- par('usr')

abline(coef)

text(usr[1]+diff(usr[1:2])*0.025, usr[3]+diff(usr[3:4])*0.1, 
     paste('y=', signif(coef[2],2), '*x +', signif(coef[1], 2), '\nadj R2=',
           signif(summary(lm)$adj.r.squared, 2)), pos=4)

ind_let <- letext(ind_let, xshift=xshift, yshift=yshift)

#---
cex <- 0.8

plot.new()
plot.window(xlim=log(range(lst_dis$dis_spat)), 
            ylim=range(sapply(lst_DDR[8:13], function(x) {
              coef <- coef(x$window$tot$fit); coef[1] + coef[2] * range(log(dis_spat))
            })))
box()
axis(1, cex.axis=cex)
axis(2, cex.axis=cex)
abline(h=0,v=0)
mtext('log of spatial distance', 1, 1.75, cex=cex)
mtext('log of the euclidean distance\nbetween two landscapes', 2, 1.75, cex=cex)

lapply(c(as.character(buf), 'env'), function(x) {
  coef <- coef(lst_DDR[[paste0(x, '_spat')]]$window$tot$fit) 
  abline(coef, col=ifelse(x %in% buf, pal_buf[x], 1))
})

ind_let <- letext(ind_let, xshift=xshift, yshift=yshift)

leg_buf(cex=0.75, cex.title=1)


lst_plot_04[['DDR']] <- recordPlot()

dev.off()


# overall fit by buffer ####

x11(width=wdt1, height = wdt1)

par(mar=c(3,4,0.5,0.5), mgp=c(2,0.75,0))
cex <- 0.8

plot.new()
plot.window(xlim=log(range(lst_dis$dis_spat)), 
            ylim=range(sapply(lst_DDR[8:13], function(x) {
              coef <- coef(x$window$tot$fit); coef[1] + coef[2] * range(log(dis_spat))
            })))
box()
axis(1, cex.axis=cex)
axis(2, cex.axis=cex)
abline(h=0,v=0)
mtext('log of spatial distance', 1, 1.75, cex=cex)
mtext('log of the euclidean distance\nbetween two landscapes', 2, 1.75, cex=cex)

lapply(c(as.character(buf), 'env'), function(x) {
  coef <- coef(lst_DDR[[paste0(x, '_spat')]]$window$tot$fit) 
  abline(coef, col=ifelse(x %in% buf, pal_buf[x], 1))
})

lst_plot_04[['lscp_vs_spat']] <- recordPlot()

dev.off()


#---

save(lst_plot_04, file=paste0(dir_out, '04_lst_plot.Rdata'))




#####
# is beta linked to the percentage of forest?

rnd <- 3

k <- NULL
var_check <- c('pland_forest','pland_open')
for(h in var_check){
  
  l <- NULL
  for(i in as.character(buf)){
    
    ll <- NULL
    dif <- c(5,10,15,20,25,50)
    par(mfrow=c(3,2))
    for(j in dif){
      n_smp <- labels(dis_bray)  
      
      p_frst <- arr_met[h,i,n_smp]
      is.simi <- ifelse(dist(p_frst) < j, T, F)
      
      cmbn <- combn(n_smp, 2)
      
      bc <- dis_bray[is.simi]
      m_frst <- apply(cmbn[,is.simi], 2, function(x) mean(p_frst[x]))
      
      #---
      lm <- lm(bc~log(m_frst+1))
      pv <- round(anova(lm)$`Pr(>F)`[1], rnd)
      coef <- round(coef(lm), rnd)
      r2 <- round(summary(lm)$adj.r.squared, rnd)
      tb <- table(is.simi)
      
      ll <- cbind(ll, c(pv,coef[2],r2, tb, tb[1]/tb[2], mean(log(m_frst+1))))
      
      # #---
      # plot(bc~log(m_frst+1), col=pal_buf[i],
      #      main=paste('buf =', i, 'simili =', j,
      #                 '\npv =', pv, 'slope =', coef[2], 'r2 =', r2))
      # abline(coef(lm))
      
    }
    
    dimnames(ll) <- list(c('pv','slp','r2','F','T','rat', 'm_frst'), dif)
    
    l <- abind(l, ll, along=3)
    
  }
  dimnames(l)[[3]] <- buf
  
  k <- abind(k, l, along=4)
}

dimnames(k)[[4]] <- var_check


plot(k['slp',,,"pland_forest"]~k['slp',,,"pland_open"], col=as.character(gl(5,6,labels=pal_buf)), pch=19)
abline(0,1)

plot(k['T',,,"pland_forest"]~k['T',,,"pland_open"], col=as.character(gl(5,6,labels=pal_buf)), pch=19)
abline(0,1)


plot(k["m_frst","5",,"pland_forest"],k["slp","5",,"pland_forest"], col=as.character(gl(5,1,labels=pal_buf)), 
     pch=19)










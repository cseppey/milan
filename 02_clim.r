#%%%%
# climatic var
#%%%%

rm(list=ls())

require(foreach)
require(doSNOW)
require(raster)
require(rgdal)


# DOWNLOAD DATA ####

dir_proj <- 'Projets/MiLan/analyse/stats/'
dir_in <- paste0(dir_proj, 'data/')
dir_out <- paste0(dir_proj, 'out/')


# load comm
load(paste0(dir_out, '00_lst_comm.Rdata'))


# retreive the bioclim data from the tiles ----

# build the stack ---

# to cite Karger, D.N., Conrad, O., Böhner, J., Kawohl, T., Kreft, H., Soria-Auza, R.W., Zimmermann, N.E.,
# Linder, P., Kessler, M. (2017). Climatologies at high resolution for the Earth land surface areas. Scientific Data.
# 4 170122. https://doi.org/10.1038/sdata.2017.122
# 
# Karger D.N., Conrad, O., Böhner, J., Kawohl, T., Kreft, H., Soria-Auza, R.W., Zimmermann, N.E,, Linder, H.P., 
# Kessler, M.. Data from: Climatologies at high resolution for the earth’s land surface areas. 
# Dryad Digital Repository.http://dx.doi.org/doi:10.5061/dryad.kd1d4

bclm <- brick(paste0(dir_in, 'map/CHELSA_bio10_01.tif'))

for(i in 2:19){
  ich <- as.character(i)
  if(i < 10){
    ich <- paste0('0',i)
  }
  bclm <- addLayer(bclm, brick(paste0(dir_in, 'map/CHELSA_bio10_', ich, '.tif')))
}

# convert smp coordinates crs to longlat
swiss.ch1903 <- "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs "
longlat <- as.character(crs(bclm))

coord_rprj <- as.data.frame(spTransform(SpatialPoints(na.omit(lst_comm$`01agg`$env[,c('X','Y')]), CRS(swiss.ch1903)), CRS(longlat)))

# get the bioclim variables for each sample
bclm_smp <- NULL
for(i in names(bclm)){
  print(i)
  bclm_smp <- cbind(bclm_smp, bclm[[i]][cellFromXY(bclm, coord_rprj)])
}

dimnames(bclm_smp) <- list(row.names(coord_rprj), paste0('bio', sapply(strsplit(names(bclm), '_'), '[[', 3)))

#---
clim <- bclm_smp

save(clim, file=paste0(dir_out, '02_clim.Rdata'))


#


















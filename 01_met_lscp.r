#%%%%
# metric of lscp
#%%%%

rm(list=ls())

require(foreach)
require(doSNOW)
require(abind)
require(raster)
require(rgdal)
require(rgeos)
require(landscapemetrics)
require(caret)

# prep cluster

nb_cpu <- parallel::detectCores()

if(exists('cl')){
  stopCluster(cl)
}

cl <- makeSOCKcluster(nb_cpu)

clusterEvalQ(cl, library(raster))
clusterEvalQ(cl, library(rgeos))
clusterEvalQ(cl, library(landscapemetrics))

registerDoSNOW(cl)

redo <- T

# DOWNLOAD DATA ####

dir_proj <- 'Projets/MiLan/analyse/stats/'
dir_in <- paste0(dir_proj, 'data/')
dir_out <- paste0(dir_proj, 'out/')

source(paste0(dir_proj, 'functions_MiLan.R'))

# load comm
load(paste0(dir_out, '00_lst_comm.Rdata'))

# map
tile <- brick(paste0(dir_in, 'map/S2GLC_T32TLS_2017_RGB.tif'))
tile <- addLayer(tile, brick(paste0(dir_in, 'map/S2GLC_T32TLS_2017.tif')))
# #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# tile <- brick('/media/cseppey/HDD/home_bkup/bkup_220822_tromso_fin/cseppey/MAP/sentinel2/preAlpes/S2GLC_T32TLS_2017/S2GLC_T32TLS_2017_RGB.tif')
# tile <- addLayer(tile, brick('/media/cseppey/HDD/home_bkup/bkup_220822_tromso_fin/cseppey/MAP/sentinel2/preAlpes/S2GLC_T32TLS_2017/S2GLC_T32TLS_2017.tif'))
# #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

info <- read.table(paste0(dir_proj, 'data/map/S2GLC_2017_legend.txt'), sep=',', skip=6)
names(info) <- c('Pixel_value', 'R', 'G', 'B', 'Opacity', 'Class_name')


# map ----

# adjust CRS
swiss.ch1903 <- "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs "
utm32 <- "+proj=utm +zone=32 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

coord_rprj <- as.data.frame(spTransform(SpatialPoints(na.omit(lst_comm$`01agg`$env[,c('X','Y')]), CRS(swiss.ch1903)), CRS(utm32)))
coord_rprj <- spTransform(SpatialPoints(na.omit(lst_comm$`01agg`$env[,c('X','Y')]), CRS(swiss.ch1903)), CRS(utm32))

# select lu and adapt the sentinel2 lu to meadow for sampling points
lu <- tile[[4]]
lu <- crop(lu, extent(unlist(attributes(extent(coord_rprj))[1:4])+5000*c(-1,1)))

lu[cellFromXY(lu, coord_rprj)] <- 102

pal_lu <- c('black',rgb(info$R/255, info$G/255, info$B/255)[-1])
names(pal_lu) <- info$Pixel_value

# legend
plot.new()
legend(0.5, 0.5, info$Class_name, fill=pal_lu, xjust=0.5, yjust=0.5)

# get the map of coarse habitat
lu_brd <- lu
lu_brd[lu_brd %in% c(62,73,75)] <- 76
lu_brd[lu_brd %in% c(82,83)] <- 84
lu_brd[lu_brd %in% c(102:106)] <- 107
lu_brd[lu_brd %in% c(121,123,162)] <- 0

unq_brd <- unique(values(lu_brd))
unq_brd <- unq_brd[unq_brd != 0]

pal_lu_brd <- lapply(info[,c('R','G','B')], function(x) tapply(x, list(c(0, 76,76,76, 84,84, 107,107,107,107,107, 0,0,0)),
                                                               function(y) round(mean(y))/255))
pal_lu_brd <- rgb(pal_lu_brd$R, pal_lu_brd$G, pal_lu_brd$B)
names(pal_lu_brd) <- c(0,76,84,107)  
  
# #%%%%%%%%
# # load lu from the old scripts
# load('~/Projets/MiLan/analyse/stats/out_bkup/01_lu_from_old.Rdata')
# #%%%%%%%%

file <- paste0(dir_out, '01_arr_met.Rdata')
if(file.exists(file) & redo == F){
  load(file)
} else {

  chunks <- round(seq(0, length(coord_rprj), length.out=nb_cpu+1))
  
  lst_chnk <- foreach(h = seq_along(chunks)[-1], .verbose=T) %dopar% {
    
    lst <- NULL
    for(i in (chunks[(h-1)]+1):chunks[h]){
      
      print(i)
      
      pt <- coord_rprj[i]
      
      metrics <- NULL
      plots <- NULL
      for(j in buf) {
        
        print(j)
        
        # buffer
        bufloop <- gBuffer(pt, width=j)
        
        bufext <- extent(bufloop)
        
        bufras <- crop(lu, bufext)
        bufras <- mask(bufras, bufloop)
        
        bufras_brd <- crop(lu_brd, bufext)
        bufras_brd <- mask(bufras_brd, bufloop)
        
        # plot
        bufrasfac <- as.factor(bufras)
        for(k in seq_along(info$Pixel_value)){
          bufrasfac[bufrasfac == info$Pixel_value[k]] <- k
        }

        bufrasfac[cellFromXY(bufrasfac, pt)] <- 7

        ind_na <- which(is.na(values(bufrasfac)))
        bufrasfac[ind_na[c(1,diff(ind_na)[-(length(ind_na)-1)]) == 1 & diff(ind_na) == 1][1:14]] <- 1:14
        
        x11(width=wdt2, height=wdt2)
        
        plot(bufrasfac, col=pal_lu)

        points(pt)
        
        plots[[as.character(j)]] <- recordPlot()
        
        dev.off()
        
        # lscp metrics
        ptch <- get_patches(bufras)
        rank102 <- which(names(ptch[[1]]) == 'class_102')
        id_ptch <- extract(ptch[[1]]$class_102, coord_rprj[i,])
        if(rank102 != 1){
          id_ptch <- id_ptch + sum(sapply(ptch[[1]][1:(rank102-1)], function(x) length(unique(values(x)[is.na(values(x)) == F]))))
        }

        # area and edge metric: lsm_p_area, lsm_c_pland, lsm_c_ed + lsm_c_pland for the other brd class
        # shape metric: lsm_p_frac
        # core area: lsm_p_cai
        # aggregation: lsm_p_enn, lsm_c_iji, lsm_c_clumpi
        # subdividion: lsm_c_mesh + lsm_c_division
        # diversity: lsm_l_sidi, lsm_l_siei

        met_pt1 <- (calculate_lsm(bufras, what=c('lsm_p_area', 'lsm_c_pland', 'lsm_c_ed',
                                                 'lsm_p_frac',
                                                 'lsm_p_cai',
                                                 'lsm_c_iji', 'lsm_c_clumpy',
                                                 'lsm_c_division',
                                                 'lsm_l_shdi')))
        met_pt2 <- (calculate_lsm(bufras_brd, what=c('lsm_c_pland')))

        # add missing broad habitat
        for(k in unq_brd){
          if(!any(met_pt2$metric == 'pland' & met_pt2$class == k)){
            met_pt2 <- rbind.data.frame(met_pt2, data.frame(layer=1, level='class', class=k, id=NA, metric='pland', value=0))
          }
        }
        met_pt2 <- met_pt2[order(met_pt2$class),]

        met_pt2$metric <- paste(met_pt2$metric, met_pt2$class, sep='_')

        met_pt <- rbind(met_pt1,met_pt2)

        # calculate the area mean
        met_pt <- rbind.data.frame(met_pt, data.frame(layer=1, level='class', class=102, id=NA, metric='area_mn',
                                                      value=mean(met_pt$value[met_pt$class == 102 & met_pt$metric == 'area'])))

        met_pt <- met_pt[(met_pt$class %in% c(102,unq_brd) | met_pt$level == 'landscape') & (met_pt$id == id_ptch | is.na(met_pt$id)),]
        met_pt <- met_pt[order(met_pt$level, met_pt$metric, met_pt$class),]
        nm <- met_pt$metric

        metrics[[as.character(j)]] <- met_pt[,c('metric','value')]
      }
      
      mat <- sapply(metrics, '[[', 2)
      row.names(mat) <- metrics[[1]]$metric
      
      lst[[i-chunks[h-1]]] <- list(mat=mat, plot=plots)
      
    }
    
    return(lst)
  }
  
  arr_met <- abind(lapply(lst_chnk, function(x) abind(lapply(x, function(y) y$mat), along=3)), along=3)
  dimnames(arr_met)[[3]] <- row.names(coord_rprj)
  
  arr_met <- arr_met[c('area','area_mn','pland','pland_76','pland_84','pland_107','ed',
                       'frac',
                       'cai',
                       'iji','clumpy',
                       'division',
                       'shdi'),,]
  
  dimnames(arr_met)[[1]][c(4:6,11)] <- c(paste0('pland_', c('anthro','forest','open')), 'clumpi')
  
  
  
  save(arr_met, lst_chnk, file=file)
  
}

for(i in as.character(buf)){
  pairs(t(arr_met[,i,]))
}

da <- dim(arr_met)

arr_cor <- NULL
for(i in as.character(buf)){
  m <- matrix(NA, da[1], da[1], dimnames=list(dimnames(arr_met)[[1]],dimnames(arr_met)[[1]]))
  for(j in 1:(da[1]-1)){
    for(k in (j+1):da[1]){
      cor <- cor.test(arr_met[j,i,],arr_met[k,i,], method='kendall')
      e <- cor$estimate
      p <- cor$p.value
      if(abs(e) > 0.8 & p < 0.001){
        m[j,k] <- e
        m[k,j] <- p
      }
    }
  }
  arr_cor <- abind(arr_cor, m, along=3)
}
dimnames(arr_cor)[[3]] <- buf


# study site ####

swiss.LV95 <- "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs"

# mask
mask <- shapefile(paste0(dir_in, 'map/calque/mask_pe_buf100m.shp'))

crs(mask) <- swiss.ch1903
mask <- spTransform(mask, utm32)

# orthophoto
ortho <- brick('~/MAP/swiss_orthophoto/SI25-2012-2013-2014.tif')
crs(ortho) <- swiss.LV95

file <- paste0(dir_out, '01_ortho_utm32.tif')
if(file.exists(file)){
  ortho_utm32 <- brick(file)
} else {
  ortho_utm32 <- projectRaster(ortho, crs=utm32)
  writeRaster(ortho_utm32, filename=file, format='GTiff')  
}

ortho_utm32 <- crop(ortho_utm32, extent(mask)) # couille dans le potage
ortho_utm32 <- mask(ortho_utm32, mask)

# plot
ortho2 <- disaggregate(ortho_utm32, 5)
extlf <- extent(355186.3,356346.6,5136115,5137360)
exthf <- extent(348154.1,348954.3,5143815,5144840)

#---
svg(paste0(dir_lscp, 'prealpe.svg'), width=7, height=5)
par(mar=rep(2,4))

plotRGB(ortho2)

points(coord_rprj, col='gold', lwd=0.5, pch=19)

dev.off()


# Swiss
svg(paste0(dir_lscp, 'swiss.svg'))

alt = raster::getData('alt', country='CHE', path='MAP/')
slope = terrain(alt, opt='slope')
aspect = terrain(alt, opt='aspect')
hill = hillShade(slope, aspect, 40, -90)

hill_rprj <- projectRaster(hill, crs=utm32)

plot(hill_rprj, legend=F, col=grey(seq(0,1,length.out=100)))
plot(mask, add=T, col=2, border=F)

dev.off()

#####























